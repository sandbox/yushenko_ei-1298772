README.txt
==========
The module adds a page with advert to the site, the advert appears after the set number of page views on the site.

	In the backend the following settings (admin/config/content/recall_ad) are available:
		
		Frequency of occurrence settings 
			- the number of pages viewed on the site, after that a page with advert appears
			- the time of page displaying before return to the last requested page (in seconds)
		
		Page look settings 
			- a link to the image with the logo 
			- the width and the height of the image 
			- the background colour  
			- the links above and under the advert 
			- the advert code


COMPATIBILITY NOTES
==================
	The module is compatible with drupal 7.8

AUTHOR/MAINTAINER
======================
http://groupbwt.com

