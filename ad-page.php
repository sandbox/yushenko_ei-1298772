<?php
	if(isset($_SESSION['recall_ad']['last_node_url'])){
		$redirect_url = $GLOBALS['base_url'] .'/'. $_SESSION['recall_ad']['last_node_url'];
	}
	else {		
		$redirect_url = $GLOBALS['base_url'];
	}
?>
<!DOCTYPE html>
<html>
<head>
<script src="<?= $GLOBALS['base_url'].'/sites/all/modules/recall_ad/js/jquery-1.6.3.min.js'?>"></script>
<style type="text/css">
	body {background: <?php echo variable_get('recall_ad_background_color'); ?>;font: Arial; }
	div.main {width:900px;  margin:20px auto; /*border:1px solid #FF0000;*/ padding: 20px; }
	img.logo {border: 1px solid #000000; float: left; margin: 7px 10px 5px 0; padding: 3px;}
	div.header {
	    font: 18px Arial;
			zoom:1; /* This enables hasLayout, which is required for older IE browsers */
  		filter: progid:DXImageTransform.Microsoft.Shadow(color='#b0b0b0', Direction=135, Strength=3);
			-moz-box-shadow:2px 2px 2px #b0b0b0;
			-webkit-box-shadow:2px 2px 2px #b0b0b0;
			box-shadow:2px 2px 2px #b0b0b0;
			margin: 5px 0 20px;
			padding: 0 0 1px; 
	}
	div.header span#counter {
			 color: #0000FF;
			 font: bold 26px Arial;
			 text-shadow:#0000FF 1px 1px 2px;
			 filter: DropShadow(Color=#0000FF, OffX=2, OffY=2, Positive=1);
	}
	p.header {
	  
	}
	div.link-above {text-align: center; margin: 50px 0 30px; font: 16px Arial;}
	div.link-below {text-align: center; margin: 30px 0 0; font: 16px Arial;}
	div.ad-area {margin:10px 110px;}

</style>
<!--[if IE 7]>
	<style type="text/css">
		div.header span#counter { padding: 0 3px 0 0; } 
	</style>	
<![endif]--> 
<!--[if IE 8]>
	<style type="text/css">
		p.header {padding: 26px 0 0 116px;}
	</style>	
<![endif]--> 
<!--[if IE 9]>
	<style type="text/css">
		p.header {padding: 26px 0 0 116px;}
	</style>	
<![endif]-->
<script type="text/javascript">
	var beg_time = <?php echo variable_get('recall_ad_namb_seconds'); ?>
	
  function settimer() {
		window.i = 0
		window.timer1 = window.setInterval("draw()", 1000)
	}

	function draw() {
			var cur = beg_time - i;
			$('span#counter').html(cur);
			if (i >= beg_time) { 
				clearInterval(window.timer1);
				var url = "<?php  echo $redirect_url;  ?>";    
				$(location).attr('href',url);
			}	
			i = i+1;
	}
	$(document).ready(function(){
		settimer(); 
		
	});
</script>
</head>
<body>
	<div class="main">
	    <div class="header">	
			<img class="logo" alt="logo" 
				<?php if(trim(variable_get('recall_ad_logo_width'))){
					    	echo 'width="'. variable_get('recall_ad_logo_width') . '" ';
							}
					  	if(trim(variable_get('recall_ad_logo_height'))){
					  		echo 'height="'. variable_get('recall_ad_logo_height'). '" ';
							}
				?>
				src="<?php
				 	    $url_img = variable_get('recall_ad_logo_url');
							$h = trim(variable_get('recall_ad_logo_height'));
							$w = trim(variable_get('recall_ad_logo_width'));
						
							$result = $GLOBALS['base_url'] . '/sites/all/modules/recall_ad/img_resize.php?url=' . $url_img;
							if($h) { $result .= '&h='. $h; }
							if($w) { $result .= '&w='. $w; }
							echo $result;
						?>" />
				<p class="header">Your page will load in <span id="counter"></span> seconds <br/>
						   <a id="next_page"  href="<?php  echo $redirect_url;  ?>">Click here</a>	</p>
				<div style="clear:both"></div>		   
	    </div>	
		<div class="link-above">
			<?php 	
					$html = variable_get('recall_ad_link_above'); 
					echo $html;
			?>	
		</div>
		<div class="ad-area">
			<?php
					$html = variable_get('recall_ad_text_ad');
					echo $html;
			?>
		</div>
		<div class="link-below">
			<?php
					$html = variable_get('recall_ad_link_below');
					echo $html;
			?>
		</div>
	</div>	
</body>
</html>
