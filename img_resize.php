<?php

		
	$filename = $_GET['url'];
	list($width, $height) = getimagesize($filename);
	
	$allowed = array('jpg','gif','png');
	$pos = strrpos($_GET['url'], ".");
	$str = substr($_GET['url'],($pos + 1)); 
	
	if($str == 'jpg' || $str == 'jpeg'){
		header("Content-type: image/jpeg");
	}
        
  if($str == 'gif'){
  	header("Content-type: image/gif");
  }
        
  if($str == 'png'){
  	header("Content-type: image/png");
  }
   
	
	if($str == 'jpg' || $str == 'jpeg'){
		$image = imagecreatefromjpeg($filename);
	}
		
	if($str == 'png'){
		$image = imagecreatefrompng($filename);
	}	
		
	if($str == 'gif'){
		$image = imagecreatefromgif($filename);
	}		
		
	
	if (isset($_GET['h']) && isset($_GET['w'])) {
		$h = $_GET['h'];
		$w = $_GET['w'];
	}
	else if (isset($_GET['h'])) {
		$h = $_GET['h'];
		$w =  round($width * ($h/$height));
	}
	else if (isset($_GET['w'])) {
		$w = $_GET['w'];
		$h = round($height * ($w/$width));
	}
	else {
		if($str == 'jpg' || $str == 'jpeg'){
			imagejpeg($image, null, 100);
		}
		if($str == 'png'){
			imagepng($image, null);
		}	
		if($str == 'gif'){
			imagegif($image, null);
		}		
		exit();
	}	
	
	
	// Resample
	$image_p = imagecreatetruecolor($w, $h);
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $w, $h, $width, $height);

	// Output
	if($str == 'jpg' || $str == 'jpeg'){
		imagejpeg($image_p, null, 100);
	}
	if($str == 'png'){
		imagepng($image_p, null);
	}	
	if($str == 'gif'){
		imagegif($image_p, null);
	}		
	
