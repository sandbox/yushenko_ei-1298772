<?php
	
/**
 * implementation form in admin page 'admin/config/content/recall_ad'
 */ 
function recall_ad_admin() {
  global $base_url;
  $form = array();
  
	$form['#attributes']['enctype'] = 'multipart/form-data';
 
	$form['recall_ad_background_color'] = array(
		'#type' => 'textfield',
		'#title' => t('Background color'),
		'#default_value' => variable_get('recall_ad_background_color'),
		'#size' => 20,
		'#maxlength' => 20,
		'#description' => t("Background color for ad page"),
		'#required' => FALSE
	);
  
  $form['recall_ad_logo_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Logo url'),
		'#default_value' => variable_get('recall_ad_logo_url'),
		'#size' => 150,
		'#maxlength' => 200,
		'#description' => t("Url to file with logo image"),
		'#required' => FALSE
	);
	
	$form['recall_ad_logo_width'] = array(
		'#type' => 'textfield',
		'#title' => t('Logo width'),
		'#default_value' => variable_get('recall_ad_logo_width'),
		'#size' => 20,
		'#maxlength' => 20,
		'#description' => t("Width logo image"),
		'#required' => FALSE
	);
	
	$form['recall_ad_logo_height'] = array(
		'#type' => 'textfield',
		'#title' => t('Logo height'),
		'#default_value' => variable_get('recall_ad_logo_height'),
		'#size' => 20,
		'#maxlength' => 20,
		'#description' => t("Height logo image"),
		'#required' => FALSE
	);
	
	$form['recall_ad_namb_visit_pages'] = array(
		'#type' => 'textfield',
		'#title' => t('Namber pageviews'),
		'#default_value' => variable_get('recall_ad_namb_visit_pages'),
		'#size' => 20,
		'#maxlength' => 20,
		'#description' => t("How many pageviews before the page pops up"),
		'#required' => FALSE
	);
	
	$form['recall_ad_namb_seconds'] = array(
		'#type' => 'textfield',
		'#title' => t('Timer'),
		'#default_value' => variable_get('recall_ad_namb_seconds'),
		'#size' => 20,
		'#maxlength' => 20,
		'#description' => t("Countdown timer setting (namber seconds)"),
		'#required' => FALSE
	);
	
	$form['recall_ad_file_logo'] = array(
		'#type' => 'file',
		'#title' => t('File logo (.jpg)'),
		'#required' => FALSE
	);
	
	$form['recall_ad_link_above'] = array(
		'#type' => 'textarea',
		'#title' => t('Link above ad'),
		'#default_value' => variable_get('recall_ad_link_above'),
		'#cols' => 62,
		'#rows' => 2,
		'#description' => t("Link above ad"),
		'#required' => FALSE
	);
	
	$form['recall_ad_link_below'] = array(
		'#type' => 'textarea',
		'#title' => t('Link below ad'),
		'#default_value' => variable_get('recall_ad_link_below'),
		'#cols' => 62,
		'#rows' => 2,
		'#description' => t("Link below ad"),
		'#required' => FALSE
	);
	
	$form['recall_ad_text_ad'] = array(
		'#type' => 'textarea',
		'#title' => t('Ad text'),
		'#default_value' => variable_get('recall_ad_text_ad'),
		'#cols' => 62,
		'#rows' => 6,
		'#description' => t("Ad text"),
		'#required' => FALSE
	);
	
  return system_settings_form($form);
}

/**
 * implementation validation form in admin page 'admin/config/content/recall_ad'
 */
function recall_ad_admin_validate($form, &$form_state) {
	global $base_url;

  $namb_visit = $form_state['values']['recall_ad_namb_visit_pages'];
  if (!is_numeric($namb_visit)||($namb_visit < 0)||($namb_visit > 2000)) {
	  form_set_error('recall_ad_namb_visit_pages', t('Invalid \'How many pageviews before the page pops up\' field'));
  }
    
  $logo_width = $form_state['values']['recall_ad_logo_width'];
  $logo_width = trim($logo_width);
  $pattern = '/^[\d]*$/';
  if (!preg_match($pattern, $logo_width)||($logo_width < 0)||($logo_width > 2000)) {
	  form_set_error('recall_ad_logo_width', t("Invalid 'Logo width' field"));
  }
  
  $logo_height = $form_state['values']['recall_ad_logo_height'];
  $logo_height = trim($logo_height);
  if (!preg_match($pattern, $logo_height)||($logo_height < 0)||($logo_height > 2000)) {
	  form_set_error('recall_ad_logo_height', t("Invalid 'Logo height' field"));
  }
  
  $namb_sec = $form_state['values']['recall_ad_namb_seconds'];
  $namb_sec = trim($namb_sec);
  $pattern1 = '/^[\d]+$/';
  if (!preg_match($pattern1, $namb_sec)||($namb_sec < 0)||($namb_sec > 2000)) {
	  form_set_error('recall_ad_namb_seconds', t("Invalid 'Timer' field"));
  }
 
  if (isset($form['recall_ad_file_logo'])) {
		$validators = array('file_validate_extensions' => array('jpeg jpg png'));
		
		if ($file = file_save_upload('recall_ad_file_logo', $validators, 'public://', FILE_EXISTS_REPLACE)) {
			$form_state['storage']['recall_ad_file_logo'] = $file;
			
			$file->status = FILE_STATUS_PERMANENT; 
			file_save($file);
			
			$extension = explode(".", $file->filename);
			$last_n = count($extension) - 1;
			$ext = $extension[$last_n];
			
			$dir =  'public://recall_ad_logo.'.$ext;
			file_move($file, $dir, FILE_EXISTS_REPLACE);
			$st = file_create_url($dir);
			
			$form_state['values']['recall_ad_logo_url'] = $st;	
		}
	} 
 
 
}


/**
 * implementation hook_menu
 */ 
function recall_ad_menu() {

  $items = array();
   
  $items['admin/config/content/recall_ad'] = array(
    'title' => t('Recall ad module settings'),
    'description' => t('Recall ad module settings (description)'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('recall_ad_admin'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM
  );
   
  $items['recall'] = array(
		'title' => t('Test page'),
		'description' => t('Test (description) page'),
		'access callback' => 'recall_ad_access_view',
		'access arguments' => array(),
		'page callback' => 'recall_ad_recall_view',
	    'type' => MENU_NORMAL_ITEM
  ); 

  return $items;
}

/**
 * access callback for item menu 'recall'
 */
function recall_ad_access_view(){
	return true;
}

/**
 * page callback for item menu 'recall'
 */
function recall_ad_recall_view($ghi = 0, $jkl = '') {
	require_once 'ad-page.php';
}

/**
 * implementation hook_modules_enabled
 */ 
function recall_ad_modules_enabled($modules) {
	global $base_url;	

	variable_set('recall_ad_background_color', '#FFFFFF');
	$st = $base_url .'/sites/all/modules/recall_ad/logo.jpg';
	variable_set('recall_ad_logo_url',$st);	
	variable_set('recall_ad_logo_width', 80);	
	variable_set('recall_ad_logo_height', 80);	
	variable_set('recall_ad_namb_visit_pages', 5);	
	variable_set('recall_ad_namb_seconds', 30);	
	variable_set('recall_ad_link_above', '<a href="http://www.google.ru">link above (google)</a>');	
	variable_set('recall_ad_link_below', '<a href="http://www.google.ru">link below (google)</a>');	
	
	$st = '<script type="text/javascript"><!-- google_ad_client = "ca-pub-9713101048283512"; google_ad_slot = "5653561121"; '.
		  'google_ad_width = 728; google_ad_height = 90; //--></script> <script type="text/javascript" '.
		  'src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>';
		  
	variable_set('recall_ad_text_ad', $st);	
	

}

/**
 * implementation hook_disable
 */
function recall_ad_disable(){
	variable_del('recall_ad_background_color');
	variable_del('recall_ad_logo_url');
	variable_del('recall_ad_logo_width');
	variable_del('recall_ad_logo_height');
	variable_del('recall_ad_namb_visit_pages');
	variable_del('recall_ad_namb_seconds');
	variable_del('recall_ad_link_above');
	variable_del('recall_ad_link_below');
	variable_del('recall_ad_text_ad');
	
}

/**
 * implementation hook_node_view
 */
function recall_ad_node_view($node, $view_mode, $langcode){
	global $base_url;
		
	if ($view_mode == 'full'){	
		if(!isset($_SESSION['recall_ad'])){
			$_SESSION['recall_ad'] = array();
		}
		
			
		$recall_ad = & $_SESSION['recall_ad'];
		$mav_count_visit = variable_get('recall_ad_namb_visit_pages');
		$cur_node_url = $base_url; 
		
		if(isset($recall_ad['last_node_id'])&&($recall_ad['count_visit'] <= $mav_count_visit)){
			$last_node_id = $recall_ad['last_node_id'];
			$cur_node_id = $node->nid;
			
			if (!($cur_node_url = drupal_lookup_path('alias',"node/".$node->nid))){
				$cur_node_url = "node/".$node->nid;
			}
			
			if($cur_node_id != $last_node_id){
				$recall_ad['last_node_id'] = $cur_node_id;
				
				$recall_ad['last_node_url'] = $cur_node_url; 
				$recall_ad['count_visit'] += 1;
			}
			
		}
    else if(isset($recall_ad['count_visit'])&&($recall_ad['count_visit'] > $mav_count_visit)){
			$recall_ad['count_visit'] = 0;
			if (!($recall_ad['last_node_url'] = drupal_lookup_path('alias',"node/".$node->nid))){
				$recall_ad['last_node_url'] = "node/".$node->nid;
			}
			
			drupal_goto("recall");
		}
		else {
			$recall_ad['last_node_id'] = $node->nid;
			$recall_ad['last_node_url'] = $cur_node_url; 
			$recall_ad['count_visit'] = 1;
		}

	}
}























